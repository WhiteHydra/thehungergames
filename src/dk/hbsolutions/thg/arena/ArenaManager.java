package dk.hbsolutions.thg.arena;

import java.util.*;

import org.bukkit.*;
import org.bukkit.inventory.*;

import dk.hbsolutions.thg.*;

public class ArenaManager{

    //save where the player teleported
    public Map<String, Location> locs = new HashMap<String, Location>();
    //make a new instance of the class
    public static ArenaManager am = new ArenaManager();
    //a few other fields
    public Map<String, ItemStack[]> inv = new HashMap<String, ItemStack[]>();
    public Map<String, ItemStack[]> armor = new HashMap<String, ItemStack[]>();
    //list of arenas
    public List<Arena> arenas = new ArrayList<Arena>();
    int arenaSize = 0;

    static Start plugin;
    public ArenaManager(Start thg) {
        plugin = thg;
    }

    public ArenaManager(){

    }

    //we want to get an instance of the manager to work with it statically
    public static ArenaManager getManager(){
        return am;
    }

    //get an Arena object from the list
    public Arena GetArena(int i){
        for(Arena a : arenas){
            if(a.GetId() == i){
                return a;
            }
        }
        return null;
    }

    //create arena
    public Arena CreateArena(Location l){
        int num = arenaSize + 1;
        arenaSize++;

        Arena a = new Arena(l, num);
        arenas.add(a);

        plugin.getConfig().set("Arenas." + num, SerializeLoc(l));
        List<Integer> list = plugin.getConfig().getIntegerList("Arenas.Arenas");
        list.add(num);
        plugin.getConfig().set("Arenas.Arenas", list);
        plugin.saveConfig();

        return a;
    }

    public Arena ReloadArena(Location l) {
        int num = arenaSize + 1;
        arenaSize++;
 
        Arena a = new Arena(l, num);
        arenas.add(a);
 
        return a;
    }
    
    public void RemoveArena(int i) {
        Arena a = GetArena(i);
        if(a == null) {
            return;
        }
        arenas.remove(a);

        plugin.getConfig().set("Arenas." + i, null);
        List<Integer> list = plugin.getConfig().getIntegerList("Arenas.Arenas");
        list.remove(i);
        plugin.getConfig().set("Arenas.Arenas", list);
        plugin.saveConfig();    
    }

    public void LoadGames(){
        arenaSize = 0;      

        if(plugin.getConfig().getIntegerList("Arenas.Arenas").isEmpty()){
            return;
        }
                
        for(int i : plugin.getConfig().getIntegerList("Arenas.Arenas")){
            Arena a = ReloadArena(DeserializeLoc(plugin.getConfig().getString("Arenas." + i)));
            a.id = i;
        }
    }

    public String SerializeLoc(Location l){
        return l.getWorld().getName()+","+l.getBlockX()+","+l.getBlockY()+","+l.getBlockZ();
    }
    public Location DeserializeLoc(String s){
        String[] st = s.split(",");
        return new Location(Bukkit.getWorld(st[0]), Integer.parseInt(st[1]), Integer.parseInt(st[2]), Integer.parseInt(st[3]));
    }
}
