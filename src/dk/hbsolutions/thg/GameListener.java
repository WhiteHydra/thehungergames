package dk.hbsolutions.thg;

import java.util.*;

import org.bukkit.*;
import org.bukkit.entity.*;
import org.bukkit.event.*;
import org.bukkit.event.entity.*;

import dk.hbsolutions.thg.player.*;

public class GameListener implements Listener{

    static List<String> playerList = new ArrayList<String>();
    static Start plugin;
    
    static StartPlayer sp = new StartPlayer();

    public GameListener(Start plugin){
        GameListener.plugin = plugin;
    }

    @EventHandler
    public void OnDamage(EntityDamageByEntityEvent e){
        if(e.getEntity() instanceof Player && playerList.contains(((Player) e.getEntity()).getName())){
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void OnDeath(PlayerDeathEvent e){
        if(sp.IsInGame(e.getEntity())){
            sp.RemovePlayer(e.getEntity());
        }
    }

    public static void Add(Player p){
        final String name = p.getName();
        playerList.add(name);
        
        Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
            @Override
            public void run(){
                playerList.remove(name);
            }
        }, 100L);
    }
}
