package dk.hbsolutions.thg;

import org.bukkit.command.*;
import org.bukkit.entity.*;
import org.bukkit.plugin.java.*;

import dk.hbsolutions.thg.arena.*;
import dk.hbsolutions.thg.player.*;
import dk.hbsolutions.thg.util.*;

public class Start extends JavaPlugin {

	static DebugLog dl = new DebugLog();
	static StartPlayer sp = new StartPlayer();
	
	public void onEnable(){
        if(!getDataFolder().exists())
            getDataFolder().mkdir();

        if(getConfig() == null)
            saveDefaultConfig();

        new ArenaManager(this);
        ArenaManager.getManager().LoadGames();
        
        StartPlayer.AssignPlayerId();
        
        getServer().getPluginManager().registerEvents(new GameListener(this), this);
        dl.Log("THG started!");
	}
	
	public void onDisable(){
        saveConfig();
        dl.Log("THG Stopped!");
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String args[]){
		if(!(sender instanceof Player)){
            sender.sendMessage("Whoa there buddy, only players may execute this!");
            return true;
        }

        Player p = (Player) sender;

        if(cmd.getName().equalsIgnoreCase("create")){
            ArenaManager.getManager().CreateArena(p.getLocation());
            p.sendMessage("Created arena at " + p.getLocation().toString());

            return true;
        }
        if(cmd.getName().equalsIgnoreCase("join")){
            if(args.length != 1){
                p.sendMessage("Insuffcient arguments!");
                return true;
            }
            int num = 0;
            try{
                num = Integer.parseInt(args[0]);
            }catch(NumberFormatException e){
                p.sendMessage("Invalid arena ID");
            }
            sp.AddPlayer(p, num);

            return true;
        }
        if(cmd.getName().equalsIgnoreCase("leave")){
            sp.RemovePlayer(p);
            p.sendMessage("You have left the arena!");

            return true;
        }
        if(cmd.getName().equalsIgnoreCase("remove")){
            if(args.length != 1){
                p.sendMessage("Insuffcient arguments!");
                return true;
            }
            int num = 0;
            try{
                num = Integer.parseInt(args[0]);
            }catch(NumberFormatException e){
                p.sendMessage("Invalid arena ID");
            }
            ArenaManager.getManager().RemoveArena(num);

            return true;
        }

        return false;
	}
}
