package dk.hbsolutions.thg.util;

import java.util.*;

public class RandomInt {
	
	public int result = 0;
	
	public int GetRandomInt(int max, int min) {
		Random rnd = new Random();
		
		result = rnd.nextInt(max) + min;
		
		return result;
	}

}
