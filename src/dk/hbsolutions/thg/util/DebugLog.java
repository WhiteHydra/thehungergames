package dk.hbsolutions.thg.util;

import java.util.logging.*;

import org.bukkit.*;

public class DebugLog {

	private Logger log = Bukkit.getLogger();
	
	public void Log(String err) {
		log.warning(err);
	}
	
}
