package dk.hbsolutions.thg.player;

import org.bukkit.entity.*;

import dk.hbsolutions.thg.arena.*;
import dk.hbsolutions.thg.util.*;

public class StartPlayer {
	
	public static int playerId = 0;
	public static String result = "";
	
	private static int max = 24;
	private static int min = 1;
	
	static DebugLog dl = new DebugLog();
	static RandomInt ri = new RandomInt();
	static ArenaManager am = new ArenaManager();

	public static void AssignPlayerId() {
		
		ri.GetRandomInt(max, min);
		
		playerId = ri.result;
		
		result = ""+playerId;
		
		dl.Log(result);
		
	}
	
    //add players to the arena, save their inventory
    public void AddPlayer(Player p, int i){
        Arena a = am.GetArena(i);//get the arena you want to join
        if(a == null){//make sure it is not null
            p.sendMessage("Invalid arena!");
            return;
        }

        a.GetPlayers().add(p.getName());//add them to the arena list of players
        am.inv.put(p.getName(), p.getInventory().getContents());//save inventory
        am.armor.put(p.getName(), p.getInventory().getArmorContents());

        p.getInventory().setArmorContents(null);
        p.getInventory().clear();

        am.locs.put(p.getName(), p.getLocation());
        p.teleport(a.spawn);//teleport to the arena spawn
    }
    
    //remove players
    public void RemovePlayer(Player p){
        Arena a = null;//make an arena
        for(Arena arena : am.arenas){
            if(arena.GetPlayers().contains(p.getName())){
                a = arena;//if the arena has the player, the arena field would be the arena containing the player
            }
            //if none is found, the arena will be null
        }
        if(a == null || !a.GetPlayers().contains(p.getName())){//make sure it is not null
            p.sendMessage("Invalid operation!");
            return;
        }

        a.GetPlayers().remove(p.getName());//remove from arena

        p.getInventory().clear();
        p.getInventory().setArmorContents(null);

        p.getInventory().setContents(am.inv.get(p.getName()));//restore inventory
        p.getInventory().setArmorContents(am.armor.get(p.getName()));

        am.inv.remove(p.getName());//remove entries from hashmaps
        am.armor.remove(p.getName());
        p.teleport(am.locs.get(p.getName()));
        am.locs.remove(p.getName());
        
        p.setFireTicks(0);
    }
    
    public boolean IsInGame(Player p){
        for(Arena a : am.arenas){
            if(a.GetPlayers().contains(p.getName()))
                return true;
        }
        return false;
    }

}
